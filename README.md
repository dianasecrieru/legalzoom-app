# legalzoom app

This is a web application that allows the user to insert bank related data, in a JSON format. Data is persisted in a MySQL Database, using Spring Data JPA. Also the user can visualize all the data from the database, sorted by expiry date (in descending order).