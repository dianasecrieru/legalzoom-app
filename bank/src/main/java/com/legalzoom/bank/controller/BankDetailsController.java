package com.legalzoom.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legalzoom.bank.model.BankDetails;
import com.legalzoom.bank.repository.BankDetailsRepository;

@RestController
@RequestMapping("/service/bankdetails")
public class BankDetailsController {
	
	@Autowired
	BankDetailsRepository bankDetailsRepository;
	
	@GetMapping("/alldetails")
	public List<BankDetails> getSortedData() {
		List<BankDetails> bankDetailsList = bankDetailsRepository.findByOrderByExpiryDateDesc();
		for(BankDetails bankDetails : bankDetailsList) {
			String newCardNo = bankDetails.getCardNo().substring(0, 4) + "-xxxx-xxxx-xxxx";
			bankDetails.setCardNo(newCardNo);
		}
		return bankDetailsList;
	}
	
	@PostMapping("/create/details")
	public BankDetails createRealm(@RequestBody BankDetails bankDetails) {
		return bankDetailsRepository.save(bankDetails);
	}

}
