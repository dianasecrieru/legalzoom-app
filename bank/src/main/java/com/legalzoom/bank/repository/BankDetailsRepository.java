package com.legalzoom.bank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.legalzoom.bank.model.BankDetails;

public interface BankDetailsRepository extends JpaRepository<BankDetails, Long> {
	public List<BankDetails> findByOrderByExpiryDateDesc();
}
