package com.legalzoom.bank.util;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DateHandler extends StdDeserializer<LocalDate>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateHandler() {
		this(null);
	}

	public DateHandler(Class<?> vc) {
		super(vc);
		// TODO Auto-generated constructor stub
	}

	@Override
	public LocalDate deserialize(JsonParser jsonParser, DeserializationContext desContext)
			throws IOException, JsonProcessingException {
		String date = "01-" + jsonParser.getText();
		System.out.println(date);
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
			LocalDate localDate = LocalDate.parse(date, formatter);
			return localDate;
		}catch(Exception e) {
			return LocalDate.now();
		}
	}
	
}
